#ifndef _THROWER_H_
#define _THROWER_H_

#include "exception.h"

void thrower(const std::string& type, const std::string& what) {
  if (type == "app") { RAISE_APP_EXCEPTION(what); }

  if (type == "std") { throw std::runtime_error(EXCEPTION_DETAILS(what)); }

  // throwing a pointer is not recommended
  if (type == "ptr") { throw new std::runtime_error(what); }

  throw std::runtime_error("Unknown exception type.");
}

#endif
