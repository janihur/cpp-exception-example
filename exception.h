#ifndef _EXCEPTION_H_
#define _EXCEPTION_H_

//
// __FILE__ and __LINE__ should be available for both MS Visual Studio 2013 and GCC 4.6.3
//
// http://msdn.microsoft.com/en-us/library/b0084kay.aspx
// http://gcc.gnu.org/onlinedocs/gcc-4.6.3/cpp/Standard-Predefined-Macros.html
//

//
// A plain macro
//
// http://stackoverflow.com/q/2670816/272735
// http://stackoverflow.com/q/5641427/272735
//

#define STRINGIZE(x) STRINGIZE2(x)
#define STRINGIZE2(x) #x
#define EXCEPTION_DETAILS(what) std::string(what) + " file: " __FILE__ " line: " STRINGIZE(__LINE__)

//
// An own exception class with a helper macro
//

#include <stdexcept>
#include <string>
#include <sstream>

#define RAISE_APP_EXCEPTION(what) throw AppException(what, __FILE__, __LINE__)

class AppException: public std::exception {
public:
  AppException(const std::string& details,
	       const std::string& file,
	       int line)
    : details_(details), file_(file),  line_(line) {}
  virtual ~AppException() throw() {}
  virtual const char* what() const throw() {
    std::ostringstream msg;
    msg << "(type = AppException)"
	<< "(details = " << details_
	<< ")(file = " << file_
	<< ")(line = " << line_ 
	<< ")";
    return msg.str().c_str(); 
  }
private:
  const std::string details_;
  const std::string file_;
  const int line_;
};

#endif
