#include <cstdlib>
#include <iostream>
#include <stdexcept>

#include "thrower.h"

int main(int argc, char* argv[]) {
  try {
    if (argc > 2 ) {
      thrower(argv[1], argv[2]);
    }
  } catch (const AppException& ex) {
    std::cerr << "APPLICATION EXCEPTION: " << std::endl 
	      << ex.what() << std::endl;
    return EXIT_FAILURE;
  } catch (const std::exception& ex) {
    std::cerr << "STANDARD EXCEPTION: " << std::endl 
	      << ex.what() << std::endl;
    return EXIT_FAILURE;
  } catch (...) {
    std::cerr << "UNIDENTIFIED EXCEPTION: " << std::endl 
	      << "No further details available." << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
